var express = require("express");
const cors = require('cors');
let app = express();
const port = process.env.PORT || 8887;

app.use(express.urlencoded({ extended: true })); //middleware portion for adding data
app.use(cors());
app.use(express.json());
const path = require('path');

function requireHTTPS(req, res, next) {
    // The 'x-forwarded-proto' check is for Heroku
    if (!req.secure && req.get('x-forwarded-proto') !== 'https') {
        return res.redirect('https://' + req.get('host') + req.url);
    }
    next();
}


app.use(requireHTTPS);


app.use(express.static('./dist/Heroke-Angular'));



app.get("/api/test", function (req, res) {
    console.log("entered inside api/test")
    res.send({ status: true, message: "Hello" })
});


app.get('/*', function (req, res) {
    console.log("entered inside /*")

    res.sendFile(path.join(__dirname + '/dist/Heroke-Angular/index.html'));
    
});


app.listen(port, () => {
    console.log("Server ready at" + port)
});
