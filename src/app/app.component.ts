import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'Heroke-Angular';
  private contactsUrl = 'api/test';
  // private contactsUrl = 'http://localhost:8887/test';

  constructor(
    private http: HttpClient
  ) { }


  backend() {
    this.http.get<any>(this.contactsUrl).subscribe(
      response => {
        let resp= response
        console.log(resp)
        console.log(resp.status)
        if (resp.status) {
          alert("Successfully Returned from backend")
        }
        else {
          alert("failed")
        }
      });
  }
}
